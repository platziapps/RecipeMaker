/*
* Project created by Alejandro Ramirez
* 27/12/19
* 09:10
* GMT -5
* */

fun main() {
    val messageDisplay = """
            Select an option.
                1. Make a recipe
                2. See my recipes
                3. Exit
                
            My option is:     
        """.trimIndent()
    val listOfIngredients = listOf(
        "Water", "Milk", "Meat", "Vegetables",
        "Fruits", "Cereal", "Eggs", "Oil"
    )
    var option: String
    var iteratorPrimary = true
    var iteratorSecundary: Boolean
    val listOfRecipes = HashMap<String, ArrayList<String>>()
    var listOfChosenIngredients: ArrayList<String>

    println("\n:: Welcome to Recipe Maker ::\n")

    while (iteratorPrimary) {
        print(messageDisplay)
        option = readLine().toString()
        iteratorSecundary = true
        when (option) {
            "1" -> {
                listOfChosenIngredients = ArrayList()
                println("Great!, You have chosen to make a recipe!")
                print("Choose a name for your recipe: ")
                val nameOfRecipe = readLine().toString()
                println("$nameOfRecipe is fantastic!, now choose the ingredients you will use:")
                listOfIngredients.forEachIndexed { index, ingredient ->
                    println("   ${index + 1}. $ingredient")
                }
                print("Choose one: ")
                iteratorSecundary@ while (iteratorSecundary) {
                    val ingredientSelected = try {
                        readLine().toString().toInt() - 1
                    } catch (e: Exception) {
                        println("You can only select the ingredients that are listed")
                        println("Try again :)")
                        print("Choose one: ")
                        continue@iteratorSecundary
                    }
                    if (listOfIngredients.indices.contains(ingredientSelected)) {
                        val item = listOfIngredients[ingredientSelected]
                        listOfChosenIngredients.add(item)
                        println("$item was added to its ingredients list!")
                        print("Do you want to add more ingredients? (y/n) : ")
                        val continueAdd = readLine().toString()
                        if (continueAdd.toLowerCase().equals("y")) {
                            print("Choose another ingredient: ")
                        } else {
                            if (!continueAdd.toLowerCase().equals("n")){
                                println("Ok, I'll take that as a no")
                            }
                            listOfRecipes.put(nameOfRecipe, listOfChosenIngredients)
                            iteratorSecundary = false
                        }
                    } else {
                        println("You can only select the ingredients that are listed")
                        println("Try again :)")
                        print("Choose one: ")
                        continue@iteratorSecundary
                    }
                }
                print("You chose: ")
                listOfChosenIngredients.forEachIndexed { index, ingredient ->
                    when (index) {
                        listOfChosenIngredients.size - 2 -> print("$ingredient and ")
                        listOfChosenIngredients.size - 1 -> print("$ingredient.\n")
                        else -> print("$ingredient, ")
                    }
                }
                println("Enjoy your $nameOfRecipe!\n")
            }
            "2" -> {
                println("You have chosen to see my recipes!")
                if (listOfRecipes.isEmpty()) {
                    println("You have not created a recipe yet\nTo see your recipes, start by creating one in the main menu\n")
                } else {
                    listOfRecipes.forEach {
                        println("${it.key}: ${it.value}")
                    }
                    println()
                }
            }
            "3" -> {
                println("See you later :)")
                iteratorPrimary = false
            }
            else -> println("You must choose one of the three options\nTry again :)\n")
        }
    }
}